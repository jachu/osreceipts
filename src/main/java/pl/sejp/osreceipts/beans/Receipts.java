/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sejp.osreceipts.beans;

import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author jachu
 */
@Named(value = "receipts")
@RequestScoped
public class Receipts {

    /**
     * Creates a new instance of Receipts
     */
    public Receipts() {
    }
    
    private String UName;

    public String getUName() {
        return UName;
    }

    public void setUName(String UName) {
        this.UName = UName;
    }
    
    @PostConstruct
    public void init() {
        this.UName = "J@chu";
        Logger.getLogger(null).info("Bean " + this.getClass().getName() + " initialized");
    }
    
    @PreDestroy
    public void bye() {
        Logger.getLogger(null).info("Bean " + this.getClass().getName() + " destroyed");
    }
    
}
